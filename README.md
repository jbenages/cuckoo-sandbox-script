# Cuckoo Sandbox Script

## Description
This script contain a copy of original with modifications to create the Cuckoo sandbox machine.

## Changes
- Changed apt packets name for compatibility with Debian 10.
- Added repository installation for mongodb.
- Added check function to check if exist Guest VM to import it only whether needs. 
